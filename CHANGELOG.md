## [2.4.1](https://gitlab.com/alice-plex/core-js/compare/2.4.0...2.4.1) (2020-04-03)


### Bug Fixes

* **core:** Fix album validating ([3f1b003](https://gitlab.com/alice-plex/core-js/commit/3f1b003))

# [2.4.0](https://gitlab.com/alice-plex/core-js/compare/2.3.0...2.4.0) (2020-04-03)


### Features

* **core:** Add sort_title to album ([26692c8](https://gitlab.com/alice-plex/core-js/commit/26692c8))
* **core:** Reduce noise on validation errors ([43ed1e6](https://gitlab.com/alice-plex/core-js/commit/43ed1e6))

# [2.3.0](https://gitlab.com/alice-plex/core-js/compare/2.2.0...2.3.0) (2020-01-12)


### Features

* **core:** Add selector to WikiScraper ([f0d2166](https://gitlab.com/alice-plex/core-js/commit/f0d2166))

# [2.2.0](https://gitlab.com/alice-plex/core-js/compare/2.1.3...2.2.0) (2020-01-09)


### Features

* **core:** Add sort_title to Artist ([846fe94](https://gitlab.com/alice-plex/core-js/commit/846fe94))

## [2.1.3](https://gitlab.com/alice-plex/core-js/compare/2.1.2...2.1.3) (2019-10-13)


### Bug Fixes

* **core:** Change collection to normalizeTitle ([bec8b7c](https://gitlab.com/alice-plex/core-js/commit/bec8b7c))

## [2.1.2](https://gitlab.com/alice-plex/core-js/compare/2.1.1...2.1.2) (2019-10-06)


### Bug Fixes

* **core:** Remove escapeUrl ([40616c4](https://gitlab.com/alice-plex/core-js/commit/40616c4))

## [2.1.1](https://gitlab.com/alice-plex/core-js/compare/2.1.0...2.1.1) (2019-10-06)


### Bug Fixes

* **core:** Wiki url not escaped correctly ([a37cd31](https://gitlab.com/alice-plex/core-js/commit/a37cd31))

# [2.1.0](https://gitlab.com/alice-plex/core-js/compare/2.0.1...2.1.0) (2019-09-26)


### Features

* **scrap:** Add createCombineScrapper ([76a93ea](https://gitlab.com/alice-plex/core-js/commit/76a93ea))

## [2.0.1](https://gitlab.com/alice-plex/core-js/compare/2.0.0...2.0.1) (2019-09-26)


### Bug Fixes

* **typings:** Fix typings not being transformed ([12d62f6](https://gitlab.com/alice-plex/core-js/commit/12d62f6))

# [2.0.0](https://gitlab.com/alice-plex/core-js/compare/1.2.0...2.0.0) (2019-09-26)


### Code Refactoring

* **core:** Move away from classes ([242015e](https://gitlab.com/alice-plex/core-js/commit/242015e))


### BREAKING CHANGES

* **core:** classes are no longer supported

# [1.2.0](https://gitlab.com/alice-plex/core-js/compare/1.1.4...1.2.0) (2019-07-25)

### Bug Fixes

- **ci:** Remove **tests** with rm instead of del-cli ([361c1fe](https://gitlab.com/alice-plex/core-js/commit/361c1fe))
- **dependencies:** Update dependencies ([22b3f51](https://gitlab.com/alice-plex/core-js/commit/22b3f51))

### Features

- **wiki:** Support `<li>` in table columns ([d5265fe](https://gitlab.com/alice-plex/core-js/commit/d5265fe))

import Ajv, { ErrorObject } from "ajv";

import _ from "lodash";

import { SerializedModel } from "@/model";
import { Schemas } from "@/schema";

enum Schema {
  Show = "show",
  Movie = "movie",
  Album = "album",
  Artist = "artist",
  Episode = "episode",
  Actor = "actor"
}

export const createAjv = (
  options: Ajv.Options = { allErrors: true, format: "full" }
): Ajv.Ajv => {
  const ajv = new Ajv(options);
  ajv.addSchema(Schemas.Definitions);
  ajv.addSchema(Schemas.Actor, Schema.Actor);
  ajv.addSchema(Schemas.Show, Schema.Show);
  ajv.addSchema(Schemas.Movie, Schema.Movie);
  ajv.addSchema(Schemas.Album, Schema.Album);
  ajv.addSchema(Schemas.Artist, Schema.Artist);
  ajv.addSchema(Schemas.Episode, Schema.Episode);
  return ajv;
};

type ValidateOptions = {
  ajv: Ajv.Ajv;
};

type Result = {
  valid: boolean;
  errors: ErrorObject[];
};

export const validateSchema = (
  data: any,
  schema: Schema,
  options: ValidateOptions
): Result => {
  const { ajv } = options;
  const validate = ajv.getSchema(schema);
  const valid = validate(data);
  return {
    valid: !!valid,
    errors: validate.errors || []
  };
};

export const toSerializedModel = (
  data: any,
  options: ValidateOptions
): SerializedModel => {
  let errors: ErrorObject[] = [];

  for (const schema of Object.values(Schema)) {
    const result = validateSchema(data, schema, options);
    if (result.valid) {
      return data;
    }
    if (
      _.isArray(result.errors) &&
      (errors.length <= 0 || errors.length > result.errors.length)
    ) {
      errors = result.errors;
    }
  }
  throw new Error(JSON.stringify(errors, null, 2));
};

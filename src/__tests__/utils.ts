import { readFileSync } from "fs";
import $ from "cheerio";
import { resolve } from "path";

const resolveDataPath = (...relativePaths: string[]): string =>
  resolve(__dirname, "..", "..", "src", "__tests__", "data", ...relativePaths);

export const readDataString = (file: string): any =>
  readFileSync(resolveDataPath(file), "utf8");

export const mockHtml = (html: string) => async (
  _url: string,
  _request: RequestInit,
  cheerio?: CheerioOptionsInterface
): Promise<CheerioStatic> => $.load(html, cheerio);

export const mockJson = (json: any) => (): Promise<any> =>
  Promise.resolve(json);

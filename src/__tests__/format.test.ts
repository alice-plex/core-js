import {
  normalize,
  normalizeTitle,
  removeLeadingSpace,
  removeSingleLinkBreak,
  removeTrailingSpace,
  replaceArrowBrackets,
  replaceContinuousNewlines,
  replaceContinuousSpace,
  replaceDot,
  replaceEndingSpace,
  replaceFullStop,
  replaceTilde
} from "@/format";

describe("format", () => {
  test("removeTrailingSpace", () => {
    const source = "test \n test  \ntest\n";
    const result = "test\n test\ntest";
    expect(removeTrailingSpace(source)).toBe(result);
  });

  test("removeLeadingSpace", () => {
    const source = " test \n test  \n  test\n";
    const result = "test \ntest  \ntest\n";
    expect(removeLeadingSpace(source)).toBe(result);
  });

  test("replaceContinuousNewlines", () => {
    const source = "test\ntest\n\ntest\n\n\ntest";
    const result = "test\ntest\n\ntest\n\ntest";
    expect(replaceContinuousNewlines(source)).toBe(result);
  });

  test("replaceContinuousSpace", () => {
    const source = "test  test \t test";
    const result = "test test test";
    expect(replaceContinuousSpace(source)).toBe(result);
  });

  test("replaceTilde", () => {
    const source = "~a~";
    const result = "〜a〜";
    expect(replaceTilde(source)).toBe(result);
  });

  test("replaceDot", () => {
    const dataSet = {
      ".": ".",
      "..": "…",
      "...": "…",
      "....": "……",
      ".....": "……",
      "......": "……",
      "........": "………",
      "……。": "……。",
      "・・・。": "…。"
    };

    Object.keys(dataSet).forEach(key => {
      expect(replaceDot(key)).toBe(dataSet[key]);
    });
  });

  test("replaceFullStop", () => {
    const source = "a．B";
    const result = "a・B";
    expect(replaceFullStop(source)).toBe(result);
  });

  test("replaceArrowBrackets", () => {
    const source = "<as>";
    const result = "〈as〉";
    expect(replaceArrowBrackets(source)).toBe(result);
  });

  test("replaceEndingSpace", () => {
    const source = "test?  test! ";
    const result = "test?test!";
    expect(replaceEndingSpace(source)).toBe(result);
  });

  test("removeSingleLinkBreak", () => {
    const source = "Hello\n\nWorld?\nTest";
    const result = "Hello\n\nWorld?Test";
    expect(removeSingleLinkBreak(source)).toBe(result);
  });

  test("normalize", () => {
    const dataSet = {
      "。 \n! ": "。!",
      "test.....": "test……",
      "Hello     World!": "Hello World!",
      "Hello World! ASD": "Hello World!ASD"
    };

    Object.keys(dataSet).forEach(key => {
      expect(normalize(key)).toBe(dataSet[key]);
    });
  });

  test("normalizeTitle", () => {
    const dataSet = {
      "。 \n! ": "。!",
      "test.....": "test……",
      "Hello     World!": "Hello World!",
      "Hello World! ASD": "Hello World! ASD"
    };

    Object.keys(dataSet).forEach(key => {
      expect(normalizeTitle(key)).toBe(dataSet[key]);
    });
  });
});

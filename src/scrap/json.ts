import { ScrapContext, ScrapFunction, ScrapResult } from "./type";
import { fetchJsonMemorized } from "./utils";

type Options<T> = {
  request?: RequestInit;
  createUrl: (context: ScrapContext) => Promise<string>;
  parse: (context: ScrapContext, json: T) => Promise<ScrapResult>;
};

export const createJsonScrapper = <T = any>(
  options: Options<T>
): ScrapFunction => async context => {
  const { createUrl, request, parse } = options;
  const url = await createUrl(context);
  const json = await fetchJsonMemorized<T>(url, request);
  return parse(context, json);
};

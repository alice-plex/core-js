import _ from "lodash";

import { Episode } from "@/model";

import { ScrapFunction } from "./type";

type EpisodeConstant = {
  [key: number]: Partial<Episode>;
};

type Options = {
  constant?: Partial<Episode>;
  episodeConstant?: EpisodeConstant;
  mergeDefault?: boolean;
};

export const createConstantScrapper = (
  options: Options = {}
): ScrapFunction => async context => {
  const { episode } = context;
  const { mergeDefault, constant = {}, episodeConstant = {} } = options;
  const hasEpisode = episode in episodeConstant;
  const episodeData = episodeConstant[episode];
  const data = !hasEpisode
    ? _.clone(constant)
    : mergeDefault
    ? _.merge({}, constant, episodeData)
    : _.clone(episodeData);
  return {
    data,
    thumbnails: []
  };
};

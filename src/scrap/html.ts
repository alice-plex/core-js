import { ScrapContext, ScrapFunction, ScrapResult } from "./type";
import { fetchDomMemorized } from "./utils";

type Options = {
  request?: RequestInit;
  cheerio?: CheerioOptionsInterface;
  createUrl: (context: ScrapContext) => Promise<string>;
  parse: (context: ScrapContext, dom: CheerioStatic) => Promise<ScrapResult>;
};

export const createHtmlScrapper = (
  options: Options
): ScrapFunction => async context => {
  const { createUrl, request, cheerio, parse } = options;
  const url = await createUrl(context);
  const dom = await fetchDomMemorized(url, request, cheerio);
  return parse(context, dom);
};

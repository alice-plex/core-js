import _ from "lodash";

import { ScrapFunction } from "./type";

export const createCombineScrapper = (
  scrappers: ScrapFunction[]
): ScrapFunction => async context => {
  const promises = scrappers.map(scrapper => scrapper(context));
  const episodes = await Promise.all(promises);
  return _.merge({}, ...episodes);
};

import { createConstantScrapper } from "@/scrap";

describe("scrap", () => {
  describe("constant", () => {
    describe("createConstantScrapper", () => {
      test("constant", async () => {
        const title = ["title"];
        const scraper = createConstantScrapper({
          constant: { title }
        });
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data.title).toEqual(title);
      });

      test("episodeConstant", async () => {
        const title = ["title"];
        const scraper = createConstantScrapper({
          episodeConstant: {
            [1]: { title }
          }
        });
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data.title).toEqual(title);
      });

      test("mergeDefault", async () => {
        const title = ["title"];
        const aired = "2019-01-01";
        const rating = 5;
        const scraper = createConstantScrapper({
          constant: { aired, rating: 10 },
          episodeConstant: {
            [1]: { title, rating }
          },
          mergeDefault: true
        });
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data.title).toEqual(title);
        expect(data.aired).toEqual(aired);
        expect(data.rating).toEqual(rating);
      });

      test("default", async () => {
        const scraper = createConstantScrapper();
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data).toEqual({});
      });
    });
  });
});

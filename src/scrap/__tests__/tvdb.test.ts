import { createTvdbScrapper } from "@/scrap";
import { fetchJsonMemorized } from "@/scrap/utils";

jest.mock("../utils");

async function mockTvdbApi(
  url: string | Request,
  init?: RequestInit
): Promise<any> {
  switch (url) {
    case "https://api.thetvdb.com/login":
      return {
        token: "token"
      };
    case "https://api.thetvdb.com/series/123/episodes":
    case "https://api.thetvdb.com/series/123/episodes?page=1": {
      const { headers = {} } = init || ({} as RequestInit);
      const { Authorization } = headers as any;
      if (Authorization !== "Bearer token") {
        return {};
      }
      return {
        links: {
          first: 1,
          last: 1,
          next: null,
          prev: null
        },
        data: [
          {
            id: 127131,
            airedSeason: 1,
            airedSeasonID: 6345,
            airedEpisodeNumber: 1,
            episodeName: "Pilot (1)",
            firstAired: "2004-09-22",
            guestStars: ["Greg Grunberg", "John Dixon", " Michelle Arthur"],
            director: "J.J. Abrams",
            directors: ["J.J. Abrams"],
            writers: ["J.J. Abrams", "Damon Lindelof"],
            overview: "overview",
            language: {
              episodeName: "en",
              overview: "en"
            },
            productionCode: "100",
            showUrl: "http://www.tv.com/episode/334467/summary.html",
            lastUpdated: 1484282112,
            dvdDiscid: "",
            dvdSeason: 1,
            dvdEpisodeNumber: 1,
            dvdChapter: null,
            absoluteNumber: null,
            filename: "episodes/73739/127131.jpg",
            seriesId: 73739,
            lastUpdatedBy: 471903,
            airsAfterSeason: null,
            airsBeforeSeason: null,
            airsBeforeEpisode: null,
            thumbAuthor: 2011,
            thumbAdded: "",
            thumbWidth: "400",
            thumbHeight: "225",
            imdbId: "tt0636289",
            siteRating: 8,
            siteRatingCount: 129
          }
        ]
      };
    }
    default:
      return {};
  }
}

describe("scrap", () => {
  describe("tvdb", () => {
    beforeAll(() => {
      // @ts-ignore
      fetchJsonMemorized.mockImplementation(mockTvdbApi);
    });

    test("scrap", async () => {
      const scraper = createTvdbScrapper({
        tvdbId: "123",
        credential: {
          apikey: "apikey",
          userkey: "userkey",
          username: "username"
        },
        fields: {
          title: true,
          aired: true,
          summary: true
        }
      });
      const { data } = await scraper({ episode: 1, season: 1 });
      expect(data.title).toEqual(["Pilot (1)"]);
      expect(data.summary).toEqual("overview");
      expect(data.aired).toEqual("2004-09-22");
    });

    test("thumbnails", async () => {
      const scraper = createTvdbScrapper({
        tvdbId: "123",
        credential: {
          apikey: "apikey",
          userkey: "userkey",
          username: "username"
        },
        fields: {
          thumbnail: true
        }
      });
      const { thumbnails } = await scraper({ episode: 1, season: 1 });
      expect(thumbnails).toEqual([
        "https://www.thetvdb.com/banners/episodes/73739/127131.jpg"
      ]);
    });
  });
});

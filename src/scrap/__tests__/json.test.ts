import { createJsonScrapper } from "@/scrap";
import { fetchJsonMemorized } from "@/scrap/utils";
import { mockJson } from "@/__tests__/utils";

jest.mock("../utils");

describe("scrap", () => {
  describe("JsonEpisodeScraper", () => {
    test("scrap", async () => {
      const title = ["title"];
      // @ts-ignore
      fetchJsonMemorized.mockImplementation(mockJson({ title }));

      const scraper = createJsonScrapper({
        async createUrl() {
          return "";
        },
        async parse(_, json) {
          return {
            data: {
              title: json.title
            },
            thumbnails: []
          };
        }
      });
      const { data } = await scraper({
        episode: 1,
        season: 1
      });

      expect(data.title).toEqual(title);
    });

    test("thumbnails", async () => {
      const url = "https://example.co/thumbnail";
      // @ts-ignore
      fetchJsonMemorized.mockImplementation(mockJson({ url }));
      const scraper = createJsonScrapper({
        async createUrl() {
          return "";
        },
        async parse(_, json) {
          return {
            data: {},
            thumbnails: [json.url]
          };
        }
      });
      const { thumbnails } = await scraper({
        episode: 1,
        season: 1
      });
      expect(thumbnails).toEqual([url]);
    });
  });
});

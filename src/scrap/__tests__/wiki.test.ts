import {
  createJPSmartWikiScrapper,
  createSmartWikiScrapper,
  createWikiScrapper
} from "@/scrap";
import { fetchDomMemorized } from "@/scrap/utils";
import { mockHtml, readDataString } from "@/__tests__/utils";

jest.mock("../utils");

describe("scrap", () => {
  describe("createWikiScrapper", () => {
    test("scrap", async () => {
      // @ts-ignore
      fetchDomMemorized.mockImplementation(
        mockHtml(
          '<table class="wikitable"> <tr> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td>Bob</td><td>Yellow</td></tr><tr> <td>Michelle</td><td>Purple</td></tr></table>'
        )
      );
      const scraper = createWikiScrapper({
        url: "https://example.com/",
        mapping: {
          title: 1
        }
      });
      const { data } = await scraper({ episode: 1, season: 1 });
      expect(data.title).toEqual(["Yellow"]);
    });

    test("scrap multipleRow", async () => {
      // @ts-ignore
      fetchDomMemorized.mockImplementation(
        mockHtml(
          '<table class="wikitable"><tr><th>Number</th> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td rowspan="2">1</td><td>Bob</td><td>Red</td></tr><tr> <td colspan="4">Alice</td></tr><tr> <td>2</td><td>Michelle</td><td>Purple</td></tr></table>'
        )
      );
      const scraper = createWikiScrapper({
        url: "https://example.com/",
        multipleRow: 0,
        mapping: {
          title: 2
        }
      });
      const { data } = await scraper({ episode: 1, season: 1 });
      expect(data.title).toEqual(["Red", "Alice"]);
    });

    describe("createSmartWikiScrapper", () => {
      test("scrap", async () => {
        // @ts-ignore
        fetchDomMemorized.mockImplementation(
          mockHtml(
            '<h1><span id="title">Title</span></h1> <table class="wikitable"> <tr> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td>Bob</td><td>Yellow</td></tr><tr> <td>Michelle</td><td>Purple</td></tr></table> <table class="wikitable"> <tr> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td>Bob2</td><td>Yellow2</td></tr><tr> <td>Michelle2</td><td>Purple2</td></tr></table>'
          )
        );

        const scraper = createSmartWikiScrapper({
          url: "https://example.com/",
          tag: "title",
          tagOffset: 1,
          mapping: {
            title: 1
          }
        });
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data.title).toEqual(["Yellow2"]);
      });

      test("scrap expect", async () => {
        // @ts-ignore
        fetchDomMemorized.mockImplementation(
          mockHtml(
            '<h1><span id="title">Title</span></h1> <table class="wikitable"> <tr> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td>Bob</td><td>Yellow</td></tr><tr> <td>Michelle</td><td>Purple</td></tr></table>'
          )
        );

        const scraper = createSmartWikiScrapper({
          url: "https://example.com/",
          tag: "title",
          expect: new Map([[[0, 0], ["Name"]]]),
          mapping: {
            title: 1
          }
        });

        const { data } = await scraper({ episode: 1, season: 1 });

        expect(data.title).toEqual(["Yellow"]);
      });

      test("scrap expect throw", async () => {
        // @ts-ignore
        fetchDomMemorized.mockImplementation(
          mockHtml(
            '<h1><span id="title">Title</span></h1> <table class="wikitable"> <tr> <th>Name</th> <th>Favorite Color</th> </tr><tr> <td>Bob</td><td>Yellow</td></tr><tr> <td>Michelle</td><td>Purple</td></tr></table>'
          )
        );

        const scraper = createSmartWikiScrapper({
          url: "https://example.com/",
          tag: "title",
          expect: new Map([[[0, 0], ["Bob"]]]),
          mapping: {
            title: 1
          }
        });

        await expect(scraper({ episode: 1, season: 1 })).rejects.toBeInstanceOf(
          Error
        );
      });
    });

    describe("createJPSmartWikiScrapper", () => {
      test("scrap", async () => {
        const html = readDataString("wiki.html");
        // @ts-ignore
        fetchDomMemorized.mockImplementation(mockHtml(html));
        const scraper = createJPSmartWikiScrapper({
          subject: "刀使ノ巫女",
          expect: new Map([[[0, 1], ["サブタイトル"]]]),
          mapping: {
            title: 1
          }
        });

        const { data } = await scraper({ episode: 1, season: 1 });

        expect(data.title).toEqual(["切っ先の向く先"]);
      });

      test("scrap(li)", async () => {
        const html = readDataString("wiki2.html");
        // @ts-ignore
        fetchDomMemorized.mockImplementation(mockHtml(html));
        const scraper = createJPSmartWikiScrapper({
          subject: "Fairy_gone_フェアリーゴーン",
          expect: new Map([[[0, 1], ["サブタイトル"]]]),
          mapping: {
            writers: 2
          }
        });
        const { data } = await scraper({ episode: 1, season: 1 });
        expect(data.writers).toEqual(["十文字青", "鈴木健一"]);
      });
    });
  });
});

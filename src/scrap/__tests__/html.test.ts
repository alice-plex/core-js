import { createHtmlScrapper } from "@/scrap";
import { fetchDomMemorized } from "@/scrap/utils";
import { mockHtml } from "@/__tests__/utils";

jest.mock("../utils");

describe("scrap", () => {
  describe("HtmlEpisodeScraper", () => {
    test("scrap", async () => {
      // @ts-ignore
      fetchDomMemorized.mockImplementation(
        mockHtml(
          '<html><head></head><body><p id="test">title</p></body></html>'
        )
      );

      const scraper = createHtmlScrapper({
        async createUrl() {
          return "";
        },
        async parse(_, dom) {
          return {
            data: {
              title: [dom("#test").text()]
            },
            thumbnails: []
          };
        }
      });
      const { data } = await scraper({
        episode: 1,
        season: 1
      });

      expect(data.title).toEqual(["title"]);
    });

    test("thumbnails", async () => {
      // @ts-ignore
      fetchDomMemorized.mockImplementation(
        mockHtml(
          '<html><head></head><body><p id="test">title</p></body></html>'
        )
      );

      const scraper = createHtmlScrapper({
        async createUrl() {
          return "";
        },
        async parse() {
          return {
            data: {},
            thumbnails: ["https://example.co/thumbnail"]
          };
        }
      });
      const { thumbnails } = await scraper({
        episode: 1,
        season: 1
      });
      expect(thumbnails).toEqual(["https://example.co/thumbnail"]);
    });
  });
});

import { URL } from "url";
import moment from "moment";
import _ from "lodash";

import { Episode } from "@/model";

import { ScrapContext, ScrapFunction } from "./type";
import { fetchJsonMemorized } from "./utils";

type TvdbCredential = {
  apikey: string;
  userkey: string;
  username: string;
};

type TvdbFields = {
  title: boolean;
  aired: boolean;
  summary: boolean;
  thumbnail: boolean;
};

export type Options = {
  tvdbId: string;
  credential: TvdbCredential;
  fields?: Partial<TvdbFields>;
  language?: string;
  transformContext?: (context: ScrapContext) => ScrapContext;
};

const fetchAuthToken = async (credential: TvdbCredential): Promise<string> => {
  const url = "https://api.thetvdb.com/login";
  const json = await fetchJsonMemorized(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(credential)
  });
  const { token } = json;
  if (!_.isString(token)) {
    throw new Error("Unable to get TVDB auth token!");
  }
  return token;
};

const getAuthHeaders = (authToken: string, language: string): HeadersInit => ({
  Accept: "application/json",
  Authorization: `Bearer ${authToken}`,
  "Accept-Language": language
});

const fetchEpisodes = async (
  tvdbId: string,
  pageNumber: number,
  request?: RequestInit
): Promise<TvdbEpisodeData[]> => {
  const url = new URL(`https://api.thetvdb.com/series/${tvdbId}/episodes`);
  url.searchParams.append("page", `${pageNumber}`);
  const json = await fetchJsonMemorized<TvdbSeriesEpisodes>(url.href, request);
  const { data, links } = json;
  const { next } = links;
  if (next) {
    const newData = await fetchEpisodes(tvdbId, next, request);
    data.push(...newData);
  }
  return data;
};

const fetchEpisodesMemorized = _.memoize(
  fetchEpisodes,
  (tvdbId: string, pageNumber: number): string =>
    JSON.stringify({ tvdbId, pageNumber })
);

const fetchEpisode = async (
  context: ScrapContext,
  options: Options
): Promise<TvdbEpisodeData> => {
  const {
    tvdbId,
    credential,
    language = "ja",
    transformContext = (c: ScrapContext) => c
  } = options;
  const { episode: airedEpisodeNumber, season: airedSeason } = transformContext(
    context
  );
  const authToken = await fetchAuthToken(credential);
  const headers = getAuthHeaders(authToken, language);
  const episodes = await fetchEpisodesMemorized(tvdbId, 1, { headers });
  const ep = _.find(episodes, { airedSeason, airedEpisodeNumber });
  if (!ep) {
    throw new Error(
      `Cannot find season ${airedSeason} episode ${airedEpisodeNumber}`
    );
  }
  return ep;
};

export const createTvdbScrapper = (
  options: Options
): ScrapFunction => async context => {
  const { fields = {} } = options;
  const { title, aired, summary, thumbnail } = fields;
  const episode = await fetchEpisode(context, options);
  const data: Partial<Episode> = {};
  const thumbnails: string[] = [];
  if (title) {
    data.title = [episode.episodeName];
  }
  if (summary) {
    data.summary = episode.overview;
  }
  if (aired) {
    const date = moment(episode.firstAired);
    data.aired = date.format("YYYY-MM-DD");
  }
  if (thumbnail) {
    const { filename } = episode;
    if (filename) {
      thumbnails.push(`https://www.thetvdb.com/banners/${filename}`);
    }
  }
  return { data, thumbnails };
};

type TvdbEpisodeData = {
  absoluteNumber: number;
  airedEpisodeNumber: number;
  airedSeason: number;
  airsAfterSeason: number;
  airsBeforeEpisode: number;
  airsBeforeSeason: number;
  director: string;
  directors: string[];
  dvdChapter: number;
  dvdDiscid: string;
  dvdEpisodeNumber: number;
  dvdSeason: number;
  episodeName: string;
  filename: string;
  firstAired: string;
  guestStars: string[];
  id: number;
  imdbId: string;
  lastUpdated: number;
  lastUpdatedBy: string;
  overview: string;
  productionCode: string;
  seriesId: string;
  showUrl: string;
  siteRating: number;
  siteRatingCount: number;
  thumbAdded: string;
  thumbAuthor: number;
  thumbHeight: string;
  thumbWidth: string;
  writers: string[];
};

type TvdbLinks = {
  first: number | null;
  last: number | null;
  next: number | null;
  previous: number | null;
};

type TvdbSeriesEpisodes = {
  data: TvdbEpisodeData[];
  links: TvdbLinks;
};

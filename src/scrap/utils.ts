import fetch from "cross-fetch";
import $ from "cheerio";
import _ from "lodash";

const addUserAgent = (request: RequestInit): RequestInit =>
  _.merge(
    {
      headers: {
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
      }
    },
    request
  );

const baseFetch = async (
  url: string,
  request: RequestInit
): Promise<Response> => {
  const res = await fetch(url, addUserAgent(request));
  if (!res.ok) {
    throw new Error(`Error response code ${res.status}`);
  }
  return res;
};

export const fetchText = async (
  url: string,
  request: RequestInit = {}
): Promise<string> => {
  const response = await baseFetch(url, request);
  return response.text();
};

export const fetchTextMemorized = _.memoize(fetchText);

export const fetchDom = async (
  url: string,
  request: RequestInit = {},
  cheerio?: CheerioOptionsInterface
): Promise<CheerioStatic> => {
  const html = await fetchText(url, request);
  return $.load(html, cheerio);
};

export const fetchDomMemorized = _.memoize(fetchDom);

export const fetchJson = async <T = any>(
  url: string,
  request: RequestInit = {}
): Promise<T> => {
  const response = await baseFetch(url, request);
  return response.json();
};

export const fetchJsonMemorized: typeof fetchJson = _.memoize(fetchJson);

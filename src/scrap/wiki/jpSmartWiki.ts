import { ScrapFunction } from "@/scrap/type";
import { createSmartWikiScrapper, Options as WikiOptions } from "./smartWiki";

type Options = Omit<WikiOptions, "url" | "tag"> & {
  subject: string;
  tag?: string;
  tagOffset?: number;
};

export const createJPSmartWikiScrapper = (options: Options): ScrapFunction => {
  const { subject, tag = "各話リスト", ...others } = options;
  return createSmartWikiScrapper({
    ...others,
    tag,
    url: `https://ja.wikipedia.org/wiki/${encodeURIComponent(subject)}`
  });
};

import { parseTable } from "@joshuaavalon/cheerio-table-parser";

import { ScrapContext, ScrapFunction } from "@/scrap/type";
import { fetchDomMemorized } from "@/scrap/utils";

import { createData, createRowMapping, Options, parseElement } from "./utils";

const fetchTable = async (
  url: string,
  tableOffset: number
): Promise<CheerioElement> => {
  const dom = await fetchDomMemorized(url);
  const tables = dom("table.wikitable").toArray();
  if (tables.length <= tableOffset) {
    throw new Error(`There are only ${tables.length} table(s).`);
  }
  return tables[tableOffset];
};

export const createWikiScrapper = (
  options: Options
): ScrapFunction => async context => {
  const {
    url,
    multipleRow,
    tableOffset = 0,
    parser = parseElement,
    getRowNumber = (context: ScrapContext) => context.episode
  } = options;
  const tableEle = await fetchTable(url, tableOffset);
  const table = parseTable(tableEle, { parser });
  const rowNum = getRowNumber(context);
  const rowMap = createRowMapping(table, multipleRow)[rowNum];
  return createData(options, table, rowMap);
};

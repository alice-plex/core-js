import { parseTable } from "@joshuaavalon/cheerio-table-parser";

import { ScrapContext, ScrapFunction } from "@/scrap/type";
import { fetchDomMemorized } from "@/scrap/utils";

import {
  checkExcept,
  createData,
  createRowMapping,
  parseElement,
  Options as WikiOptions
} from "./utils";

export type Options = {
  tag: string;
  tagOffset?: number;
  expect?: Map<number[], string[]>;
  selector?: (options: Options, dom: CheerioStatic) => CheerioElement;
} & Omit<WikiOptions, "tableOffset">;

const selectTable = (options: Options, dom: CheerioStatic): CheerioElement => {
  const { tag, tagOffset = 0 } = options;
  const tables = dom(`#${tag}`)
    .parent()
    .nextAll("table.wikitable")
    .toArray();
  if (tables.length <= tagOffset) {
    throw new Error(`There are only ${tables.length} table(s).`);
  }
  return tables[tagOffset];
};

const fetchTable = async (options: Options): Promise<CheerioElement> => {
  const { url, selector = selectTable } = options;
  const dom = await fetchDomMemorized(url);
  return selector(options, dom);
};

export const createSmartWikiScrapper = (
  options: Options
): ScrapFunction => async context => {
  const {
    expect,
    multipleRow,
    parser = parseElement,
    getRowNumber = (context: ScrapContext) => context.episode
  } = options;
  const tableEle = await fetchTable(options);
  const table = parseTable(tableEle, { parser });
  if (expect) {
    checkExcept(table, expect);
  }
  const rowNum = getRowNumber(context);
  const rowMap = createRowMapping(table, multipleRow)[rowNum];
  return createData(options, table, rowMap);
};

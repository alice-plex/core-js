import $ from "cheerio";
import moment from "moment";
import _ from "lodash";

import { Episode } from "@/model";
import { normalize } from "@/format";
import { ScrapContext, ScrapResult } from "@/scrap/type";

type WikiMapping = {
  title: number;
  aired: number;
  directors: number;
  writers: number;
};

export type Options = {
  url: string;
  tableOffset?: number;
  mapping?: Partial<WikiMapping>;
  multipleRow?: number;
  parser?: (element: CheerioElement) => string[];
  parseTitle?: (values: string[]) => string[];
  parseAired?: (values: string[]) => string;
  parseDirectors?: (values: string[]) => string[];
  parseWriters?: (values: string[]) => string[];
  getRowNumber?: (context: ScrapContext) => number;
};

export const parseElement = (element: CheerioElement): string[] => {
  $("sup", element).remove();
  $("style", element).remove();
  $("ruby", element).each((_, ruby): void => {
    if ($("rp", ruby).length > 0) {
      return;
    }
    $("rt", ruby)
      .prepend("(")
      .append(")");
  });
  $("br", element).replaceWith("\n");
  $("hr", element).replaceWith("\n");
  $("li", element).append("\n");
  return [$(element).text()];
};

export const createRowMapping = (
  table: string[][][],
  multipleRow?: number
): number[][] => {
  if (_.isUndefined(multipleRow)) {
    return _.range(table.length).map(i => [i]);
  }
  const rowMap: number[][] = [];
  let index = 0;
  let prev: string[] = [];
  table.forEach((cols, row) => {
    if (!_.isArray(rowMap[index])) {
      rowMap[index] = [];
    }
    const current = cols[multipleRow];
    if (_.eq(current, prev) || row === 0) {
      rowMap[index].push(row);
    } else {
      index++;
      rowMap[index] = [row];
    }
    prev = current;
  });
  return rowMap;
};

const parseSimpleString = (values: string[]): string[] =>
  values.map(v => normalize(v).replace(/\n+/g, " "));
const parseSimpleDate = (values: string[]): string =>
  moment(values[0]).format("YYYY-MM-DD");
const parseSpaceString = (values: string[]): string[] => {
  const result = _.flatMap(values, value =>
    value
      .split(/\s+/)
      .filter(v => v)
      .map(normalize)
  );
  return _.uniq(result);
};

export const createData = (
  options: Options,
  table: string[][][],
  rowMap: number[]
): ScrapResult => {
  const {
    mapping = {},
    parseTitle = parseSimpleString,
    parseAired = parseSimpleDate,
    parseDirectors = parseSpaceString,
    parseWriters = parseSpaceString
  } = options;
  const { title, aired, directors, writers } = mapping;
  const data: Partial<Episode> = {};
  const values: string[][] = [];
  rowMap.forEach(row => {
    table[row].forEach((col, i) => {
      if (!_.isArray(values[i])) {
        values[i] = [];
      }
      values[i] = [...values[i], ...col];
    });
  });
  if (_.isNumber(title)) {
    data.title = parseTitle(values[title]);
  }
  if (_.isNumber(aired)) {
    data.aired = parseAired(values[aired]);
  }
  if (_.isNumber(directors)) {
    data.directors = parseDirectors(values[directors]);
  }
  if (_.isNumber(writers)) {
    data.writers = parseWriters(values[writers]);
  }
  return {
    data,
    thumbnails: []
  };
};

export const checkExcept = (
  table: string[][][],
  expect: Map<number[], string[]>
): void => {
  expect.forEach((value, [row, col]) => {
    const actual = table[row][col];
    if (!_.isEqual(actual, value)) {
      throw new Error(`Expect ${value} at ${row}:${col}. Actual: ${actual}`);
    }
  });
};

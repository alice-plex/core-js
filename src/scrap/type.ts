import { Episode } from "@/model";

export type ScrapContext = { season: number; episode: number };
export type ScrapResult = {
  data: Partial<Episode>;
  thumbnails: string[];
};
export type ScrapFunction = (context: ScrapContext) => Promise<ScrapResult>;

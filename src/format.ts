export const removeTrailingSpace = (value: string): string =>
  value.replace(/[^\S\n]+$/gm, "").replace(/\s+$/g, "");

export const removeLeadingSpace = (value: string): string =>
  value.replace(/^[^\S\n]+/gm, "");

export const replaceContinuousNewlines = (value: string): string =>
  value.replace(/\n{3,}/g, "\n\n");

export const replaceContinuousSpace = (value: string): string =>
  value.replace(/[^\S\n]{2,}/g, " ");

export const replaceTilde = (value: string): string =>
  value.replace(/[~˜⁓∼∽∿〜～]/g, "〜");

export const replaceDot = (value: string): string =>
  value
    .replace(/[.．、・]{3}(?![.．、・](?:[^.．、・]|$))|[.．、・]{2}/g, "…")
    .replace(/[。、]{3}(?![。、](?:[^。、]|$))|[。、]{2}/g, "…");

export const replaceFullStop = (value: string): string =>
  value.replace("．", "・");

export const replaceArrowBrackets = (value: string): string =>
  value.replace(/[＜<](.*)[>＞]/g, "〈$1〉");

export const replaceEndingSpace = (value: string): string =>
  value.replace(/([?!。])[^\S\n]+/g, "$1");

export const removeSingleLinkBreak = (value: string): string =>
  value.replace(/(?<!\n)\n(?!\n)/g, "");

export const normalize = (value: string): string => {
  let result = value.normalize("NFKC");
  result = removeTrailingSpace(result);
  result = removeLeadingSpace(result);
  result = replaceContinuousNewlines(result);
  result = replaceContinuousSpace(result);
  result = replaceEndingSpace(result);
  result = removeSingleLinkBreak(result);
  result = replaceTilde(result);
  result = replaceDot(result);
  result = replaceArrowBrackets(result);
  return result.trim();
};

export const normalizeTitle = (value: string): string => {
  let result = value.normalize("NFKC");
  result = removeTrailingSpace(result);
  result = removeLeadingSpace(result);
  result = replaceContinuousNewlines(result);
  result = replaceContinuousSpace(result);
  result = removeSingleLinkBreak(result);
  result = replaceTilde(result);
  result = replaceDot(result);
  result = replaceArrowBrackets(result);
  return result.trim();
};

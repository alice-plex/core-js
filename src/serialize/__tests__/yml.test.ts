import { deserializeYaml, serializeYaml } from "@/serialize";
import { readDataString } from "@/__tests__/utils";

describe("serialize", () => {
  describe("yml", () => {
    test("normal", () => {
      const yaml = readDataString("show.yaml");
      const obj = deserializeYaml(yaml);
      if (obj === null) {
        expect(obj).not.toBeNull();
        return;
      }
      const newYaml = serializeYaml(obj);
      expect(newYaml).toEqual(yaml);
    });

    test("invalid", () => {
      expect(() => deserializeYaml("")).toThrow();
    });

    test("float", () => {
      const yaml = readDataString("show.float.yaml");
      const obj = deserializeYaml(yaml);
      const newYaml = serializeYaml(obj);
      expect(newYaml).toEqual(yaml);
    });
  });
});

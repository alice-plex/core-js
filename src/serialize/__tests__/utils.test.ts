import { convertSeasonSummary } from "@/serialize";

describe("serialize", () => {
  describe("base", () => {
    test("convertSeasonSummary", (): void => {
      const season = convertSeasonSummary({ 2: "B", 1: "A", b: "D" } as any);
      const expected = new Map([[1, "A"], [2, "B"]]);
      expect(season).toEqual(expected);
    });
  });
});

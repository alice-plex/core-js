import * as yaml from "yaml";
import { nullOptions, strOptions } from "yaml/types";

strOptions.fold.lineWidth = 9999999999;
nullOptions.nullStr = "";

import {
  deserializeModel,
  Model,
  normalizeModel,
  serializeModel
} from "@/model";
import { createAjv, toSerializedModel } from "@/validate";

import { convertSeasonSummary } from "./utils";

export type Options = {
  removeTrailingSpace?: boolean;
};

export const serializeYaml = (model: Model, options: Options = {}): string => {
  const { removeTrailingSpace = true } = options;
  const serializedValues = serializeModel(normalizeModel(model));
  if ("season_summary" in serializedValues) {
    // eslint-disable-next-line @typescript-eslint/camelcase
    serializedValues.season_summary = convertSeasonSummary(
      serializedValues.season_summary
    ) as any;
  }
  const str = yaml.stringify(serializedValues);
  const doc = yaml.parseDocument(str);
  doc.contents.items.forEach((i: any): void => {
    switch (i.key.value) {
      case "summary":
        if (i.value) {
          i.value.type = "BLOCK_LITERAL";
        }
        break;
      case "season_summary":
        i.value.items.forEach((summary: any): void => {
          summary.value.type = "BLOCK_LITERAL";
        });
        break;
      case "rating":
        if (i.value) {
          i.value.minFractionDigits = 1;
        }
        break;
    }
  });
  let yml = String(doc);
  if (removeTrailingSpace) {
    yml = yml.replace(/[^\S\n]+\n/g, "\n");
  }
  return yml;
};

export const deserializeYaml = (str: string): Model => {
  const data = yaml.parse(str);
  const ajv = createAjv();
  const model = toSerializedModel(data, { ajv });
  return deserializeModel(model);
};

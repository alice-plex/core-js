type NumberKey = { key: string; value: number };

export function convertSeasonSummary(seasonSummary: {
  [key: number]: string;
}): Map<number, string> {
  const result = new Map<number, string>();
  const keys = Object.keys(seasonSummary)
    .map((key): NumberKey => ({ key, value: parseInt(key) }))
    .filter((key): boolean => Number.isInteger(key.value) && key.value >= 0)
    .sort((a, b): number =>
      a.value === b.value ? 0 : a.value > b.value ? 1 : -1
    );
  keys.forEach((key): void => {
    result.set(key.value, seasonSummary[key.key]);
  });
  return result;
}

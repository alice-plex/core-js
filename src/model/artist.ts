/* eslint-disable @typescript-eslint/camelcase */
import { normalize } from "@/format";
import { Model, SerializedModel } from "./model";

export type Artist = {
  sortTitle: string;
  genres: string[];
  collections: string[];
  summary: string | null;
  similar: string[];
};

export type SerializedArtist = Omit<Artist, "sortTitle"> & {
  sort_title: string;
};

export const isArtist = (model: Model): model is Artist => "similar" in model;

export const isSerializedArtist = (
  model: SerializedModel
): model is SerializedArtist => "similar" in model;

const sort = ({
  sort_title,
  genres,
  collections,
  summary,
  similar
}: SerializedArtist): SerializedArtist => ({
  sort_title,
  genres,
  collections,
  summary,
  similar
});

export const serializeArtist = (data: Artist): SerializedArtist => {
  const { sortTitle: sort_title, ...others } = data;
  return sort({ sort_title, ...others });
};

export const deserializeArtist = (data: SerializedArtist): Artist => {
  const { sort_title: sortTitle, ...others } = data;
  return { sortTitle, ...others };
};

export const normalizeArtist = (data: Artist): Artist => {
  const { genres, collections, similar, summary, sortTitle, ...others } = data;
  return {
    ...others,
    sortTitle: sortTitle ? normalize(sortTitle) : sortTitle,
    summary: summary ? normalize(summary) : summary,
    genres: genres.map(normalize),
    similar: similar.map(normalize),
    collections: collections.map(normalize)
  };
};

/* eslint-disable @typescript-eslint/camelcase */
import _ from "lodash";
import { normalize, normalizeTitle } from "@/format";

import {
  Actor,
  deserializeActor,
  normalizeActor,
  serializeActor
} from "./actor";
import {
  mapContentRating,
  mapSeasonSummary,
  mapSerializedContentRating,
  mapSerializedSeasonSummary,
  mapSerializedTitle,
  mapTitle,
  Model,
  SerializedContentRating,
  SerializedModel,
  SerializedSeasonSummary,
  SerializedTitle
} from "./model";

export type Show = {
  title: string;
  sortTitle: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired: string | null;
  summary: string | null;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  seasonSummary: { [key: number]: string };
};

export type SerializedShow = SerializedSeasonSummary<
  SerializedTitle<SerializedContentRating<Show>>
>;

export const isShow = (model: Model): model is Show => "seasonSummary" in model;

export const isSerializedShow = (
  model: SerializedModel
): model is SerializedShow =>
  "sort_title" in model && "season_summary" in model;

const sort = ({
  title,
  sort_title,
  original_title,
  content_rating,
  tagline,
  studio,
  aired,
  summary,
  rating,
  genres,
  collections,
  actors,
  season_summary
}: SerializedShow): SerializedShow => ({
  title,
  sort_title,
  original_title,
  content_rating,
  tagline,
  studio,
  aired,
  summary,
  rating,
  genres,
  collections,
  actors,
  season_summary
});

export const serializeShow = (data: Show): SerializedShow => {
  const { actors, ...others } = data;
  return sort(
    mapSerializedSeasonSummary(
      mapSerializedTitle(
        mapSerializedContentRating({
          ...others,
          actors: actors.map(serializeActor)
        })
      )
    )
  );
};
export const deserializeShow = (data: SerializedShow): Show => {
  const { actors, ...others } = data;
  return mapSeasonSummary<Show>(
    mapTitle(
      mapContentRating({
        ...others,
        actors: actors.map(deserializeActor)
      })
    )
  );
};

export const normalizeShow = (data: Show): Show => {
  const {
    title,
    sortTitle,
    originalTitle,
    tagline,
    summary,
    studio,
    genres,
    collections,
    actors,
    seasonSummary,
    ...others
  } = data;
  return {
    ...others,
    title: normalizeTitle(title),
    sortTitle: normalizeTitle(sortTitle),
    originalTitle: originalTitle.map(normalize),
    tagline: tagline.map(normalize),
    studio: studio.map(normalize),
    summary: summary ? normalize(summary) : summary,
    genres: genres.map(normalize),
    collections: collections.map(normalizeTitle),
    actors: actors.map(normalizeActor),
    seasonSummary: _.mapValues(seasonSummary, value => normalize(value))
  };
};

/* eslint-disable @typescript-eslint/camelcase */
import { normalize, normalizeTitle } from "@/format";

import {
  mapContentRating,
  mapSerializedContentRating,
  Model,
  SerializedContentRating,
  SerializedModel
} from "./model";

export type Episode = {
  title: string[];
  aired: string | null;
  contentRating: string;
  summary: string | null;
  directors: string[];
  writers: string[];
  rating: number | null;
};

export type SerializedEpisode = SerializedContentRating<Episode>;

export const isEpisode = (model: Model): model is Episode =>
  "directors" in model && !("sortTitle" in model);

export const isSerializedEpisode = (
  model: SerializedModel
): model is SerializedEpisode =>
  "directors" in model && !("sort_title" in model);

const sort = ({
  title,
  aired,
  content_rating,
  summary,
  directors,
  writers,
  rating
}: SerializedEpisode): SerializedEpisode => ({
  title,
  aired,
  content_rating,
  summary,
  directors,
  writers,
  rating
});

export const serializeEpisode = (data: Episode): SerializedEpisode =>
  sort(mapSerializedContentRating(data));
export const deserializeEpisode = (data: SerializedEpisode): Episode =>
  mapContentRating(data);

export const normalizeEpisode = (data: Episode): Episode => {
  const { title, directors, writers, summary, ...others } = data;
  return {
    ...others,
    title: title.map(normalizeTitle),
    summary: summary ? normalize(summary) : summary,
    directors: directors.map(normalize),
    writers: writers.map(normalize)
  };
};

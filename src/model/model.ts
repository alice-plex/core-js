/* eslint-disable @typescript-eslint/camelcase */
import { Actor, SerializedActor } from "./actor";
import { Album, SerializedAlbum } from "./album";
import { Artist, SerializedArtist } from "./artist";
import { Episode, SerializedEpisode } from "./episode";
import { Movie, SerializedMovie } from "./movie";
import { SerializedShow, Show } from "./show";

export type SerializedContentRating<T extends { contentRating: string }> = Omit<
  T,
  "contentRating"
> & {
  content_rating: string;
};

export type SerializedTitle<
  T extends { sortTitle: string; originalTitle: string[] }
> = Omit<T, "sortTitle" | "originalTitle"> & {
  sort_title: string;
  original_title: string[];
};

export type SerializedSeasonSummary<
  T extends { seasonSummary: { [key: number]: string } }
> = Omit<T, "seasonSummary"> & {
  season_summary: { [key: number]: string };
};

export const mapSerializedContentRating = <T extends { contentRating: string }>(
  data: T
): SerializedContentRating<T> => {
  const { contentRating: content_rating, ...others } = data;
  return { ...others, content_rating };
};

export const mapSerializedTitle = <
  T extends { sortTitle: string; originalTitle: string[] }
>(
  data: T
): SerializedTitle<T> => {
  const {
    sortTitle: sort_title,
    originalTitle: original_title,
    ...others
  } = data;
  return { ...others, sort_title, original_title };
};

export const mapSerializedSeasonSummary = <
  T extends { seasonSummary: { [key: number]: string } }
>(
  data: T
): SerializedSeasonSummary<T> => {
  const { seasonSummary: season_summary, ...others } = data;
  return { ...others, season_summary };
};
export const mapContentRating = <T extends { contentRating: string }>(
  data: SerializedContentRating<T>
): T => {
  const { content_rating: contentRating, ...others } = data;
  return { ...others, contentRating } as any;
};

export const mapTitle = <
  T extends { sortTitle: string; originalTitle: string[] }
>(
  data: SerializedTitle<T>
): T => {
  const {
    sort_title: sortTitle,
    original_title: originalTitle,
    ...others
  } = data;
  return { ...others, sortTitle, originalTitle } as any;
};

export const mapSeasonSummary = <
  T extends { seasonSummary: { [key: number]: string } }
>(
  data: SerializedSeasonSummary<T>
): T => {
  const { season_summary: seasonSummary, ...others } = data;
  return { ...others, seasonSummary } as any;
};

export type Model = Episode | Actor | Album | Artist | Movie | Show;
export type SerializedModel =
  | SerializedEpisode
  | SerializedActor
  | SerializedAlbum
  | SerializedArtist
  | SerializedMovie
  | SerializedShow;

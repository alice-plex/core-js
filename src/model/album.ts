/* eslint-disable @typescript-eslint/camelcase */
import { normalize } from "@/format";
import { Model, SerializedModel } from "./model";

export type Album = {
  sortTitle: string;
  aired: string;
  collections: string[];
  genres: string[];
  summary: string | null;
};

export type SerializedAlbum = Omit<Album, "sortTitle"> & {
  sort_title: string;
};

export const isAlbum = (model: Model): model is Album =>
  "aired" in model && !("actors" in model) && !("directors" in model);

export const isSerializedAlbum = (
  model: SerializedModel
): model is SerializedAlbum =>
  "aired" in model && !("actors" in model) && !("directors" in model);

const sort = ({
  sort_title,
  aired,
  collections,
  genres,
  summary
}: SerializedAlbum): SerializedAlbum => ({
  sort_title,
  aired,
  collections,
  genres,
  summary
});

export const serializeAlbum = (data: Album): SerializedAlbum => {
  const { sortTitle: sort_title, ...others } = data;
  return sort({ sort_title, ...others });
};
export const deserializeAlbum = (data: SerializedAlbum): Album => {
  const { sort_title: sortTitle, ...others } = data;
  return { sortTitle, ...others };
};

export const normalizeAlbum = (data: Album): Album => {
  const { genres, collections, summary, ...others } = data;
  return {
    ...others,
    summary: summary ? normalize(summary) : summary,
    genres: genres.map(normalize),
    collections: collections.map(normalize)
  };
};

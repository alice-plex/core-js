import { normalize } from "@/format";
import { Model, SerializedModel } from "./model";

export type Actor = {
  name: string;
  role: string;
  photo: string | null | undefined;
};

export type SerializedActor = Actor;

export const isActor = (model: Model): model is Actor => "role" in model;

export const isSerializedActor = (
  model: SerializedModel
): model is SerializedActor => "role" in model;

const sort = ({ name, role, photo }: SerializedActor): SerializedActor => ({
  name,
  role,
  photo
});

export const serializeActor = (data: Actor): SerializedActor => sort(data);
export const deserializeActor = (data: SerializedActor): Actor => data;

export const normalizeActor = (data: Actor): Actor => {
  const { name, role, ...others } = data;
  return { ...others, name: normalize(name), role: normalize(role) };
};

/* eslint-disable @typescript-eslint/camelcase */
import { normalize, normalizeTitle } from "@/format";

import {
  Actor,
  deserializeActor,
  normalizeActor,
  serializeActor
} from "./actor";
import {
  mapContentRating,
  mapSerializedContentRating,
  mapSerializedTitle,
  mapTitle,
  Model,
  SerializedContentRating,
  SerializedModel,
  SerializedTitle
} from "./model";

export type Movie = {
  title: string;
  sortTitle: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired: string | null;
  summary: string | null;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  directors: string[];
  writers: string[];
};

export type SerializedMovie = SerializedTitle<SerializedContentRating<Movie>>;

export const isMovie = (model: Model): model is Movie =>
  "sortTitle" in model && !("seasonSummary" in model);

export const isSerializedMovie = (
  model: SerializedModel
): model is SerializedMovie =>
  "sort_title" in model && !("season_summary" in model);

const sort = ({
  title,
  sort_title,
  original_title,
  content_rating,
  tagline,
  studio,
  aired,
  summary,
  rating,
  genres,
  collections,
  actors,
  directors,
  writers
}: SerializedMovie): SerializedMovie => ({
  title,
  sort_title,
  original_title,
  content_rating,
  tagline,
  studio,
  aired,
  summary,
  rating,
  genres,
  collections,
  actors,
  directors,
  writers
});

export const serializeMovie = (data: Movie): SerializedMovie => {
  const { actors, ...others } = data;
  return sort(
    mapSerializedTitle(
      mapSerializedContentRating({
        ...others,
        actors: actors.map(serializeActor)
      })
    )
  );
};
export const deserializeMovie = (data: SerializedMovie): Movie => {
  const { actors, ...others } = data;
  return mapTitle<Movie>(
    mapContentRating({
      ...others,
      actors: actors.map(deserializeActor)
    })
  );
};

export const normalizeMovie = (data: Movie): Movie => {
  const {
    title,
    sortTitle,
    originalTitle,
    tagline,
    summary,
    studio,
    genres,
    collections,
    actors,
    directors,
    writers,
    ...others
  } = data;
  return {
    ...others,
    title: normalizeTitle(title),
    sortTitle: normalizeTitle(sortTitle),
    originalTitle: originalTitle.map(normalize),
    tagline: tagline.map(normalize),
    studio: studio.map(normalize),
    summary: summary ? normalize(summary) : summary,
    genres: genres.map(normalize),
    collections: collections.map(normalizeTitle),
    actors: actors.map(normalizeActor),
    directors: directors.map(normalize),
    writers: writers.map(normalize)
  };
};

import {
  Actor,
  deserializeActor,
  isActor,
  isSerializedActor,
  normalizeActor,
  serializeActor,
  SerializedActor
} from "./actor";
import {
  Album,
  deserializeAlbum,
  isAlbum,
  isSerializedAlbum,
  normalizeAlbum,
  serializeAlbum,
  SerializedAlbum
} from "./album";
import {
  Artist,
  deserializeArtist,
  isArtist,
  isSerializedArtist,
  normalizeArtist,
  serializeArtist,
  SerializedArtist
} from "./artist";
import {
  deserializeEpisode,
  Episode,
  isEpisode,
  isSerializedEpisode,
  normalizeEpisode,
  SerializedEpisode,
  serializeEpisode
} from "./episode";
import {
  deserializeMovie,
  isMovie,
  isSerializedMovie,
  Movie,
  normalizeMovie,
  SerializedMovie,
  serializeMovie
} from "./movie";
import {
  deserializeShow,
  isSerializedShow,
  isShow,
  normalizeShow,
  SerializedShow,
  serializeShow,
  Show
} from "./show";
import { Model, SerializedModel } from "./model";

export function serializeModel(data: Actor): SerializedActor;
export function serializeModel(data: Album): SerializedAlbum;
export function serializeModel(data: Artist): SerializedArtist;
export function serializeModel(data: Episode): SerializedEpisode;
export function serializeModel(data: Movie): SerializedMovie;
export function serializeModel(data: Show): SerializedShow;
export function serializeModel(data: Model): SerializedModel;
export function serializeModel(data: Model): SerializedModel {
  if (isActor(data)) {
    return serializeActor(data);
  }
  if (isAlbum(data)) {
    return serializeAlbum(data);
  }
  if (isArtist(data)) {
    return serializeArtist(data);
  }
  if (isEpisode(data)) {
    return serializeEpisode(data);
  }
  if (isMovie(data)) {
    return serializeMovie(data);
  }
  if (isShow(data)) {
    return serializeShow(data);
  }
  return data;
}

export function normalizeModel(data: Actor): Actor;
export function normalizeModel(data: Album): Album;
export function normalizeModel(data: Artist): Artist;
export function normalizeModel(data: Episode): Episode;
export function normalizeModel(data: Movie): Movie;
export function normalizeModel(data: Show): Show;
export function normalizeModel(data: Model): Model;
export function normalizeModel(data: Model): Model {
  if (isActor(data)) {
    return normalizeActor(data);
  }
  if (isAlbum(data)) {
    return normalizeAlbum(data);
  }
  if (isArtist(data)) {
    return normalizeArtist(data);
  }
  if (isEpisode(data)) {
    return normalizeEpisode(data);
  }
  if (isMovie(data)) {
    return normalizeMovie(data);
  }
  if (isShow(data)) {
    return normalizeShow(data);
  }
  return data;
}

export function deserializeModel(data: SerializedEpisode): Episode;
export function deserializeModel(data: SerializedActor): Actor;
export function deserializeModel(data: SerializedAlbum): Album;
export function deserializeModel(data: SerializedArtist): Artist;
export function deserializeModel(data: SerializedMovie): Movie;
export function deserializeModel(data: SerializedShow): Show;
export function deserializeModel(data: SerializedModel): Model;
export function deserializeModel(data: SerializedModel): Model {
  if (isSerializedActor(data)) {
    return deserializeActor(data);
  }
  if (isSerializedAlbum(data)) {
    return deserializeAlbum(data);
  }
  if (isSerializedArtist(data)) {
    return deserializeArtist(data);
  }
  if (isSerializedEpisode(data)) {
    return deserializeEpisode(data);
  }
  if (isSerializedMovie(data)) {
    return deserializeMovie(data);
  }
  if (isSerializedShow(data)) {
    return deserializeShow(data);
  }
  return data;
}

export { Model, SerializedModel } from "./model";
export { Actor, SerializedActor } from "./actor";
export { Album, SerializedAlbum } from "./album";
export { Artist, SerializedArtist } from "./artist";
export { Episode, SerializedEpisode } from "./episode";
export { Movie, SerializedMovie } from "./movie";
export { SerializedShow, Show } from "./show";

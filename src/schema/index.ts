import Definitions from "./defs.schema.json";
import Actor from "./actor.schema.json";
import Show from "./show.schema.json";
import Movie from "./movie.schema.json";
import Album from "./album.schema.json";
import Artist from "./artist.schema.json";
import Episode from "./episode.schema.json";

export const Schemas = {
  Definitions,
  Actor,
  Show,
  Movie,
  Album,
  Artist,
  Episode
};

# @aliceplex/core

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

## Installation

```
npm i @aliceplex/core
```

[license]: https://gitlab.com/alice-plex/core-js/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/alice-plex/core-js/pipelines
[pipelines_badge]: https://gitlab.com/alice-plex/core-js/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/alice-plex/core-js/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@aliceplex/core
[npm_badge]: https://img.shields.io/npm/v/@aliceplex/core/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
